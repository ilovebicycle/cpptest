//
//  Class_A.h
//  CppTest
//
//  Created by Katsuhiro Komatsu on 2013/10/29.
//  Copyright (c) 2013年 Katsuhiro Komatsu. All rights reserved.
//

#ifndef __CppTest__Class_A__
#define __CppTest__Class_A__

#include <iostream>
#include <string>
using std::string;

class Class_A {
public:
    string func(void);
};

class Class_B : Class_A {
public:
    string func(void);
};

#endif /* defined(__CppTest__Class_A__) */
