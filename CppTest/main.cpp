//
//  main.cpp
//  CppTest
//
//  Created by Katsuhiro Komatsu on 2013/10/29.
//  Copyright (c) 2013年 Katsuhiro Komatsu. All rights reserved.
//

#include <iostream>
#include "Class_A.h"

int main(int argc, const char * argv[])
{
    using std::cout;
    using std::endl;
    
    Class_A *a = new Class_A();
    Class_B *b = new Class_B();
    Class_A *c = (Class_A*)new Class_B();
    
    /*
     * JavaとC++で異なるところは、継承ーオーバライドした関数の結果である。
     * Javaの場合は、常にオーバライド先の関数が呼び出されるが、
     * C++だと宣言した型（クラス）によって異なる。
     * 下記のcとcxがその例で、変数の型はClass_A、実体はClass_Bである。
     * cのケースではClass_Aの関数が呼び出されるが、cxのようにすると
     * Class_Bが呼び出される。
     */
     
    cout << "a:" << a->func() << endl;      // Class-A
    cout << "b:" << b->func() << endl;      // Class-B
    cout << "c:" << c->func() << endl;      // Class-A
    cout << "cx:"<< ((Class_B*)c)->Class_B::func() << endl; // Class-B
    
    return 0;
}

