//
//  Class_A.cpp
//  CppTest
//
//  Created by Katsuhiro Komatsu on 2013/10/29.
//  Copyright (c) 2013年 Katsuhiro Komatsu. All rights reserved.
//

#include "Class_A.h"

string Class_A::func(void) {
    return "Class-A";
}

string Class_B::func(void) {
    return "Class-B";
}

// 日本語のコメント
